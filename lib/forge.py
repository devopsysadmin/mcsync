import os
from subprocess import run, Popen, PIPE
from main import path

class Forge(object):
    def __init__(self, manager):
        self.manager = manager
        self.installer = None
        self.file = None

    def delete(self):
        if self.file:
            for f in [ self.file, f"{self.file}.log" ]:
                if os.path.exists(f):
                    os.remove(f)

    def wait_installer(self):
        print('Waiting Forge to end installation...', end='', flush=True)
        self.installer.communicate()
        print('done!')
    
    def install(self):
        self.installer = Popen(['java', '-jar', self.file], stdout=PIPE)

    def update(self, remote):
        mc, forge = remote.get('minecraft'), remote.get('forge')
        print('New forge version!')
        file = f"forge-{mc}-{forge}-installer.jar"
        self.manager.download(
            f"https://maven.minecraftforge.net/net/minecraftforge/forge/{mc}-{forge}/{file}",
            path(file)
        )
        self.file = file

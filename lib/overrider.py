import os
from shutil import copy
from lib.commons import *

class Overrider(object):

    def files(self):
        for root, _, files in os.walk('overrides'):
            dn = root.replace('overrides', '')
            if dn: dn = dn[1:]
            for file in files:
                fn = f"{dn}/{file}" if dn else file
                if not os.path.isfile(fn):
                    if dn and not os.path.isdir(dn):
                        os.makedirs(dn)
                    copy(f"overrides/{fn}", fn)

    def inline(self, filename, replacements):
        uri = path(filename)
        contents = readfile(uri).split('\n')
        for key, value in replacements.items():
            match = [ idx for idx, line in enumerate(contents) if key in line ]
            if match:
                contents[match[0]] = value
        writefile(uri, '\n'.join(contents))

#!/usr/bin/env python3

from lib import *
import json
import os
from shutil import copy
import platform
from subprocess import Popen
import sys

cfg = 'mcsync.json'
osplatform = platform.system().lower()
# realpath() <- full path (with file)
# dirname() <- only path, withoutfile
# dirname() <- previous directory
os.environ['MCSYNC_PATH'] = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
PATH = os.getenv('MCSYNC_PATH')

def osconfig(key):
    return {
        'linux' : {
            'binary' : 'minecraft',
            'data' : os.getenv('HOME')
        },
        'windows' : {
            'binary' : 'minecraft.exe',
            'data' : os.getenv('APPDATA')
        }
    }.get(osplatform).get(key)


def load_config():
    config = json.loads(read_or_create(path(cfg), json.dumps(settings.config)))
    if config == settings.config:
        exit('A default config file was created. Please fill it with proper values')
    else:
        return config


def save(contents):
    with open(path(cfg), 'w') as fn:
        fn.write(json.dumps(contents, indent=2))


def launcher(manager):
    mkdir('launcher')
    name = osconfig('binary')
    manager.download(f"launcher/{osplatform}/{name}", path(f"launcher/{name}"))
    if osplatform == 'linux':
        os.chmod(f"launcher/{name}", 0o755)


def download(manager, folder, filelist, always):
    if not os.path.exists(path(folder)):
        os.makedirs(path(folder))
    for file in filelist:
        dn = os.path.join(PATH, folder, os.path.dirname(file))
        if dn and not os.path.isdir(dn):
            os.makedirs(dn)
        uri = f"{folder}/{file}"
        if always == True or not os.path.isfile(path(uri)):
            manager.download(uri, path(uri))


def main():
    local = load_config()
    manager = Manager(local.get('url'))
    remote = json.loads(manager.get('latest.json'))

    forge = Forge(manager)
    profile = Profile(osconfig('data'), remote['profile']['name'])

    if local.get('forge', 0) != remote['forge']:
        forge.update(remote)
        forge.install()

    # Download contents while forge might be installing
    launcher(manager)
    for folder in ('overrides',):
        download(manager, folder, remote[folder], True)
    for folder in ('mods', 'shaderpacks', 'resourcepacks', 'defaults'):
        download(manager, folder, remote[folder], False)

    if local.get('forge', 0) != remote['forge']:
        forge.wait_installer()
        profile.reload()
        local['forge'] = remote['forge']
        save(local)

    if not profile.exists():
        profile.create()
    profile.update(remote, gamedir=PATH)

    # Delete older mods
    for file in [ x.name for x in os.scandir(path('mods')) if x.name not in remote['mods'] ]:
        if os.path.isfile(path(f"mods/{file}")):
            os.remove(path(f"mods/{file}"))

    # Move overrides and edit inlines
    overrider = Overrider()
    overrider.files()
    for inline in remote['inlines']:
        overrider.inline(inline['file'], inline['replacements'])

    if remote.get('title') != local.get('title'):
        local['title'] = remote.get('title')
        save(local)

    # Launch minecraft!
    forge.delete()
    Popen([path(f"launcher/{osconfig('binary')}")])
    sys.exit()


if __name__ == '__main__':
    main()

from tkinter import ttk

class ProgressBar(object):
    def __init__(self, window, **kwargs):

        self.pb = ttk.Progressbar(window.window, **kwargs)

    def increase(self, value):
        if self.pb['value'] < 100.0:
            self.pb['value'] += value

    def grid(self, **kwargs):
        self.pb.grid(**kwargs)

def main_windows():

    # Main Window
    window = Window(geometry='640x480', title='Updater', grid=True)

    # Progress Bar
    pb = ProgressBar(window, orient='horizontal', mode='determinate', length=280)
    pb.grid(column=0, row=0, columnspan=2, padx=10, pady=20)

    # Start button
    sb = Button(window, text='Start', on_click=lambda: pb.increase(1) )
    sb.grid()

    # Start!
    window.mainloop()
